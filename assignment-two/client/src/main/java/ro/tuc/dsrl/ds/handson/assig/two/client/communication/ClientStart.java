package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;
import ro.tuc.dsrl.ds.handson.assig.two.rpc.Naming;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-client
 * @Since: Sep 24, 2015
 * @Description: Starting point of the Client application.
 */
public class ClientStart {
	private static final Log LOGGER = LogFactory.getLog(ClientStart.class);

	private ClientStart() {
	}

	public static void main(String[] args) {
		ITaxService taxService = null;
		IPriceService priceService = null;
		ServerConnection connection = null;
		try {
			connection = ServerConnection.getInstance();
			taxService = Naming.lookup(ITaxService.class, connection);
			priceService = Naming.lookup(IPriceService.class, connection);

			System.out.println("Tax value: " + taxService.computeTax(new Car(2009, 2000, 10, 115, 0.0)));
			System.out.println("Selling price: " + priceService.computeSellingPrice(new Car(2014, 0, 0, 0, 2000)));
			// System.out.println(taxService.computeTax(new Car(2009, -1, 10,
			// 115, 0.0)));
		} catch (IOException e) {
			LOGGER.error("", e);
		} finally {
			if (connection != null) {
				connection.closeAll();
			}
		}
	}
}
