package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;

public class PriceService implements IPriceService {

	@Override
	public double computeSellingPrice(Car c) {
		double purchasingPrice = c.getPurchasingPrice();
		int yearDifference = 2015 - c.getYear();
		double result = purchasingPrice - (purchasingPrice / 7 * yearDifference);
		return result;
	}

}
