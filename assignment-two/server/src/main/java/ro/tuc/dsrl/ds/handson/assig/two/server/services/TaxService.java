package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-server
 * @Since: Sep 1, 2015
 * @Description:
 * 	Class used for computation of taxes to be paid for a specific car. An instance
 * 	of this class is published in the Registry so that it can be remotely accessed
 * 	by a client.
 */
public class TaxService implements ITaxService {

	public double computeTax(Car c) {
		// Dummy formula
		if (c.getStrokeCapacity() <= 0) {
			throw new IllegalArgumentException("Stroke capacity must be positive.");
		}
		return c.getStrokeCapacity() / 200.0 * 40;
	}
}
