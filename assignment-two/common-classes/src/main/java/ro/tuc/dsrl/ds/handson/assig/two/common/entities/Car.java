package ro.tuc.dsrl.ds.handson.assig.two.common.entities;

import java.io.Serializable;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-common-classes
 * @Since: Sep 1, 2015
 * @Description:
 * 	Serializable car, abstraction of a Car.
 */
public class Car implements Serializable {

	private static final long serialVersionUID = 1L;
	private int year;
	private int strokeCapacity;
	private double emissions;
	private int hp;
	private double purchasingPrice;

	public Car() {
	}

	public Car(int year, int strokeCapacity, double emissions, int hp, double purchasingPrice) {
		this.year = year;
		this.strokeCapacity = strokeCapacity;
		this.emissions = emissions;
		this.hp = hp;
		this.purchasingPrice = purchasingPrice;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getStrokeCapacity() {
		return strokeCapacity;
	}

	public void setStrokeCapacity(int strokeCapacity) {
		this.strokeCapacity = strokeCapacity;
	}

	public double getEmissions() {
		return emissions;
	}

	public void setEmissions(double emissions) {
		this.emissions = emissions;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}
	
	public double getPurchasingPrice() {
		return purchasingPrice;
	}
	
	public void setPurchasingPrice(double purchasingPrice) {
		this.purchasingPrice = purchasingPrice;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Car car = (Car) o;

		if (year != car.year || strokeCapacity != car.strokeCapacity || Double.compare(car.emissions, emissions) != 0) {
			return false;
		}

		return hp == car.hp;
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = year;
		result = 31 * result + strokeCapacity;
		temp = Double.doubleToLongBits(emissions);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + hp;
		return result;
	}

	@Override
	public String toString() {
		return "Car{" + "year=" + year + ", strokeCapacity=" + strokeCapacity + ", emissions=" + emissions + ", hp="
				+ hp + ", purchasingPrice=" + purchasingPrice + '}';
	}
}
