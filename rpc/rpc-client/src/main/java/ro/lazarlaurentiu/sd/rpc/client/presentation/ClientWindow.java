package ro.lazarlaurentiu.sd.rpc.client.presentation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * @author Lazar Laurentiu
 *
 *         Client window.
 */
public class ClientWindow extends JFrame {

	private JTextField fabricationYear;
	private JTextField engineCapacity;
	private JTextField purchasingPrice;
	private JButton computeTax;
	private JButton computeSellingPrice;
	private JTextField resultText;

	private static final long serialVersionUID = 1L;

	public ClientWindow() {
		setLayout(new GridBagLayout());

		GridBagConstraints gridBagConstraints = new GridBagConstraints();

		gridBagConstraints.insets = new Insets(5, 5, 5, 5);

		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		add(Box.createRigidArea(new Dimension(35, 5)), gridBagConstraints);

		// fabrication year
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		JLabel label = new JLabel("Fabrication year:    ");
		add(label, gridBagConstraints);
		gridBagConstraints.gridx = 2;
		fabricationYear = new JTextField(10);
		add(fabricationYear, gridBagConstraints);

		// engine capacity
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		label = new JLabel("Engine capacity:    ");
		add(label, gridBagConstraints);
		gridBagConstraints.gridx = 2;
		engineCapacity = new JTextField(10);
		add(engineCapacity, gridBagConstraints);

		// engine capacity
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		label = new JLabel("Purchasing price:    ");
		add(label, gridBagConstraints);
		gridBagConstraints.gridx = 2;
		purchasingPrice = new JTextField(10);
		add(purchasingPrice, gridBagConstraints);

		// compute tax button
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		computeTax = new JButton("Compute tax");
		add(computeTax, gridBagConstraints);

		// compute selling price button
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.gridwidth = 2;
		computeSellingPrice = new JButton("Compute selling price");
		add(computeSellingPrice, gridBagConstraints);

		// result
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = 2;
		resultText = new JTextField();
		resultText.setEditable(false);
		resultText.setHorizontalAlignment(JTextField.CENTER);
		add(resultText, gridBagConstraints);

		setTitle("RPC - RMI Client");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public String getFabricationYear() {
		return fabricationYear.getText();
	}

	public String getEngineCapacity() {
		return engineCapacity.getText();
	}

	public String getPurchasingPrice() {
		return purchasingPrice.getText();
	}

	public void registerEventHandlerForTaxButton(ActionListener actionListener) {
		computeTax.addActionListener(actionListener);
	}

	public void registerEventHandlerForSellingPriceButton(ActionListener actionListener) {
		computeSellingPrice.addActionListener(actionListener);
	}

	public void displayTaxResult(double result) {
		resultText.setText("Tax: " + result);
	}

	public void displaySellingPriceResult(double result) {
		resultText.setText("Selling price: " + result);
	}

	public void displayError(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
	}

}
