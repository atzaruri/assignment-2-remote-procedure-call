package ro.lazarlaurentiu.sd.rpc.client;

import ro.lazarlaurentiu.sd.rpc.client.controllers.SellingPriceController;
import ro.lazarlaurentiu.sd.rpc.client.controllers.TaxController;
import ro.lazarlaurentiu.sd.rpc.client.presentation.ClientWindow;

public class StartClient {

	public static void main(String[] args) {
		ClientWindow clientWindow = new ClientWindow();

		clientWindow.registerEventHandlerForTaxButton(new TaxController(clientWindow));
		clientWindow.registerEventHandlerForSellingPriceButton(new SellingPriceController(clientWindow));
	}

}
