package ro.lazarlaurentiu.sd.rpc.client.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;

import ro.lazarlaurentiu.sd.rpc.client.presentation.ClientWindow;
import ro.lazarlaurentiu.sd.rpc.commons.entities.Car;
import ro.lazarlaurentiu.sd.rpc.commons.serviceinterfaces.IPriceService;

public class SellingPriceController implements ActionListener {

	private ClientWindow window;
	private IPriceService priceService;

	public SellingPriceController(ClientWindow controlledWindow) {
		this.window = controlledWindow;

		try {
			priceService = (IPriceService) Naming.lookup("rmi://localhost/IPriceService");
		} catch (Exception e) {
			e.printStackTrace();
			window.displayError("Could not find PriceService on server! Exiting ..");
			System.exit(1);
		}
	}

	public void actionPerformed(ActionEvent event) {
		int fabricationYear;
		double purchasingPrice;

		String fabricationYearFromUser = window.getFabricationYear();
		try {
			fabricationYear = Integer.parseInt(fabricationYearFromUser);
		} catch (NumberFormatException e) {
			window.displayError("Invalid fabrication year!");
			return;
		}

		if (fabricationYear < 0) {
			window.displayError("Fabrication year should be a positive number!");
			return;
		}

		String purchasingPriceFromUser = window.getPurchasingPrice();
		try {
			purchasingPrice = Double.parseDouble(purchasingPriceFromUser);
		} catch (NumberFormatException e) {
			window.displayError("Invalid purchasing price!");
			return;
		}

		if (purchasingPrice < 0.0) {
			window.displayError("Purchasing price should be a positive real number!");
			return;
		}

		double sellingPrice = 0.0;
		try {
			sellingPrice = priceService.computeSellingPrice(new Car(fabricationYear, 0, purchasingPrice));
			window.displaySellingPriceResult(sellingPrice);
		} catch (Exception e) {
			e.printStackTrace();
			window.displayError("Error when computing selling price!");
		}
	}

}
