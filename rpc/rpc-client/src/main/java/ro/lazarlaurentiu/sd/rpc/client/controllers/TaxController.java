package ro.lazarlaurentiu.sd.rpc.client.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;

import ro.lazarlaurentiu.sd.rpc.client.presentation.ClientWindow;
import ro.lazarlaurentiu.sd.rpc.commons.entities.Car;
import ro.lazarlaurentiu.sd.rpc.commons.serviceinterfaces.ITaxService;

public class TaxController implements ActionListener {

	private ClientWindow window;
	private ITaxService taxService;

	public TaxController(ClientWindow controlledWindow) {
		this.window = controlledWindow;

		try {
			taxService = (ITaxService) Naming.lookup("rmi://localhost/ITaxService");
		} catch (Exception e) {
			e.printStackTrace();
			window.displayError("Could not find TaxService on server! Exiting ..");
			System.exit(1);
		}
	}

	public void actionPerformed(ActionEvent event) {
		int engineCapacity;
		String engineCapacityFromUser = window.getEngineCapacity();

		try {
			engineCapacity = Integer.parseInt(engineCapacityFromUser);
		} catch (NumberFormatException e) {
			window.displayError("Invalid engine capacity!");
			return;
		}

		if (engineCapacity < 1) {
			window.displayError("Engine capacity should be a positive number!");
			return;
		}

		double tax = 0.0;
		try {
			tax = taxService.computeTax(new Car(0, engineCapacity, 0));
			window.displayTaxResult(tax);
		} catch (Exception e) {
			e.printStackTrace();
			window.displayError("Error when computing tax!");
		}
	}

}
