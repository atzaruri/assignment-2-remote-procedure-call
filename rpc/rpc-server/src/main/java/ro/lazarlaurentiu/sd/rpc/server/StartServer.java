package ro.lazarlaurentiu.sd.rpc.server;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;

import ro.lazarlaurentiu.sd.rpc.server.services.PriceService;
import ro.lazarlaurentiu.sd.rpc.server.services.TaxService;

public class StartServer {

	public static void main(String[] args) {
		try {
			Naming.bind("rmi://localhost/ITaxService", new TaxService());
			Naming.bind("rmi://localhost/IPriceService", new PriceService());

			System.out.println("Server has started successfully!");
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			e.printStackTrace();
		}
	}

}
