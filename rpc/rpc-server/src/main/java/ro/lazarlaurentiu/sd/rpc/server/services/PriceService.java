package ro.lazarlaurentiu.sd.rpc.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import ro.lazarlaurentiu.sd.rpc.commons.entities.Car;
import ro.lazarlaurentiu.sd.rpc.commons.serviceinterfaces.IPriceService;

/**
 * @author Lazar Laurentiu
 * 
 *         Price service implementation.
 */
public class PriceService extends UnicastRemoteObject implements IPriceService {

	private static final long serialVersionUID = 1L;

	public PriceService() throws RemoteException {
		super();
	}

	public double computeSellingPrice(Car c) throws RemoteException {
		double purchasingPrice = c.getPurchasingPrice();
		int yearDiff = 2015 - c.getFabricationYear();
		double result = purchasingPrice - (purchasingPrice / 7 * yearDiff);
		return result;
	}

}
