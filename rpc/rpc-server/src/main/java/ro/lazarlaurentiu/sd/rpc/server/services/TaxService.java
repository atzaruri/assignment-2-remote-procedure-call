package ro.lazarlaurentiu.sd.rpc.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import ro.lazarlaurentiu.sd.rpc.commons.entities.Car;
import ro.lazarlaurentiu.sd.rpc.commons.serviceinterfaces.ITaxService;

/**
 * @author Lazar Laurentiu
 * 
 *         Tax service implementation.
 */
public class TaxService extends UnicastRemoteObject implements ITaxService {

	private static final int[] ENGINE_CAPACITY_LIMITS = new int[] { 0, 1600, 2000, 2600, 3000, 99999 };
	private static final int[] SUM_FOR_ENGINE_CAPACITY = new int[] { 8, 18, 72, 144, 290 };
	private static final int INVALID_SUM = -1;

	private static final long serialVersionUID = 1L;

	public TaxService() throws RemoteException {
		super();
	}

	public double computeTax(Car c) throws RemoteException {
		int sum = getSum(c.getEngineCapacity());

		if (sum == INVALID_SUM) {
			throw new RemoteException("Invalid engine capacity.");
		}

		return c.getEngineCapacity() / 200.0 * sum;
	}

	private int getSum(int engineCapacity) {
		for (int index = 0; index < ENGINE_CAPACITY_LIMITS.length - 1; ++index) {
			if (ENGINE_CAPACITY_LIMITS[index] < engineCapacity && engineCapacity <= ENGINE_CAPACITY_LIMITS[index + 1]) {
				return SUM_FOR_ENGINE_CAPACITY[index];
			}
		}
		return INVALID_SUM;
	}

}
