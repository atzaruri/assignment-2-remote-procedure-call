package ro.lazarlaurentiu.sd.rpc.commons.serviceinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import ro.lazarlaurentiu.sd.rpc.commons.entities.Car;

/**
 * @author Lazar Laurentiu
 *
 *         Interface for price service.
 */
public interface IPriceService extends Remote {

	public double computeSellingPrice(Car c) throws RemoteException;

}
