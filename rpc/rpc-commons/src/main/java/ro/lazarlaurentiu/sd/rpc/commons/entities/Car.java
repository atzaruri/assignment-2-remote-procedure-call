package ro.lazarlaurentiu.sd.rpc.commons.entities;

import java.io.Serializable;

/**
 * @author Lazar Laurentiu
 * 
 *         Entity for a car.
 */
public class Car implements Serializable {

	private int fabricationYear;
	private int engineCapacity;
	private double purchasingPrice;

	private static final long serialVersionUID = 1L;

	public Car(int fabricationYear, int engineCapacity, double purchasingPrice) {
		super();
		this.fabricationYear = fabricationYear;
		this.engineCapacity = engineCapacity;
		this.purchasingPrice = purchasingPrice;
	}

	public int getFabricationYear() {
		return fabricationYear;
	}

	public void setFabricationYear(int fabricationYear) {
		this.fabricationYear = fabricationYear;
	}

	public int getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(int engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public double getPurchasingPrice() {
		return purchasingPrice;
	}

	public void setPurchasingPrice(double purchasingPrice) {
		this.purchasingPrice = purchasingPrice;
	}

}
