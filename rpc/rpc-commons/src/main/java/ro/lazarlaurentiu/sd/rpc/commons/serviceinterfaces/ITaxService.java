package ro.lazarlaurentiu.sd.rpc.commons.serviceinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import ro.lazarlaurentiu.sd.rpc.commons.entities.Car;

/**
 * @author Lazar Laurentiu
 *
 *         Interface for tax service.
 */
public interface ITaxService extends Remote {

	public double computeTax(Car c) throws RemoteException;

}
